#+BEGIN_EXPORT html
---
layout: post
title: Portugal - INIAV - National Institute for Agrarian and Veterinarian Research I. P. / Instituto Nacional de Investigação Agrária e Veterinária, I. P. 
date: 2021-02-01
category: partner
---
#+END_EXPORT

#+attr_html: :width 200px :class rounded-border black-border
[[file:{{ site.baseurl}}/img/iniav.png]]
  
* Presentation
  
National Institute for Agrarian and Veterinarian Research I. P. / Instituto Nacional de Investigação Agrária e Veterinária, I. P. (INIAV)

INIAV is the Portuguese public research institute under the Ministry of Agriculture, Forestry and Rural Development whose mission is to develop applied research activities in the areas of crop, forestry and animal production, and food safety. It also provides technical and analytical services to the Agricultural, Forestry and Veterinary Sectors, in particular soil, fertilizers and plant analyses. The Research Strategic Unit of Agricultural and Forestry Systems and Plant Health (UEIS-SAFSV)

includes a research team that develops main activities in the Environmental and Natural Resources area, particularly soil and water conservation, soil productivity, and plant mineral nutrition. Under this scope, the specific research areas in UEIS-SAFSV are as follows:

- Soil and water conservation and biological resources preservation (soil characterization; soil quality indicators; nutrient dynamics in the soil, especially C, N and P; soil carbon sequestration; soil salinization risks as related to irrigation water quality; gas emissions into the atmosphere).

- Plant mineral nutrition and crop fertilization (crop macro- and micronutrients use efficiencies; agronomic value of organic by-products; evaluation of symbiotic N2 fixation and mycorrhization by pulses and pasture legumes; management of N and P in Vulnerable Zones to groundwater and surface waters contamination).

- Soil microbiology (association plant-microorganism; diversity of Rhizobium-legumes symbiosis; Plant Growth Promoting Rhizobacteria, isolated from different non-legumes hosts, mainly, N2 fixing and P-solubilizing bacteria).
  
* Contact
  - National Institute for Agrarian and Veterinarian Research I. P. / Instituto Nacional de Investigação Agrária e Veterinária, I. P.  (INIAV) - [[http://www.iniav.pt][http://www.iniav.pt]]
  -   
  - Agricultural and Forestry Systems and Plant Health ( UEIS-SAFSV) 
  -   
  
** Nádia Castanheira
   - nadia.castanheira[AT]iniav.pt

** Maria Gonçalves
   - maria.goncalves[AT]iniav.pt

** Ana Marta Paz
   - ana.paz[AT]iniav.pt

