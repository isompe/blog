#+BEGIN_EXPORT html
---
layout: post
title: France - INRAE - France's National Research Institute for Agriculture, Food and Environment
date: 2021-02-01
category: partner
---
#+END_EXPORT

#+attr_html: :width 200px :class rounded-border black-border
[[file:{{ site.baseurl}}/img/logo-inrae.jpg]]
  
* Presentation
  
INRAE, National French Research Institute for Agriculture, Food and Environment, employs 11500 staff members working on Climate change and risks, groecology, Biodiversity, Food and global health, Bioeconomy and Society and regional strategies. Two INRAE research units are involved in the i-SoMPE project: TSCF and UR SOLS. TSCF has been working for many years on soil compaction and agricultural equipment impact on soils. It even carried out projects in conjunction with tyre manufacturers to increase the efficiency of tyres while reducing their impact. It contributed to defining the ENTAM European reference framework on tyres. TSCF works also on the measurement and modelling of compaction, based on several original experimental devices (mono-wheels, OCPS...). UR SOLS works on interactions between soils physical properties and their hydric and biogeochemical functions. UR SOLS is in close relation with Arvalis , and the scientific interest group GCHP2E (promoting design and evaluation of innovative farming systems).
  
* Contact
  - France's National Research Institute for Agriculture, Food and Environment (INRAE) - [[https://www.inrae.fr/][https://www.inrae.fr/]]
  - Agroecosystems (Agroecosystems) - [[https://www.inrae.fr/en/divisions/agroecosystem][https://www.inrae.fr/en/divisions/agroecosystem]]
  - UR SOLS (UR SOLS) - [[https://www6.val-de-loire.inrae.fr/ur-sols_eng/][https://www6.val-de-loire.inrae.fr/ur-sols_eng/]]
  -   
  
** Marine Lacoste
   - marine.lacoste[AT]inrae.fr

** Myriam Chanet
   - myriam.chanet[AT]inrae.fr

