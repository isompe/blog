#+BEGIN_EXPORT html
---
layout: post
title: B - screencast basic usage of limesurvey (fill in data)
date: 2021-07-05
category: help
---
#+END_EXPORT

Here is the link [[https://www.youtube.com/watch?v=Q5dv81xzPoM]]. Use it if the video here after does not work properly.

#+HTML: <p><iframe width="560" height="315" src="https://www.youtube.com/watch?v=Q5dv81xzPoM" frameborder="0" allowfullscreen></iframe></p>
